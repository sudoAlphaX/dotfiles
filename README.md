# My Dotfiles

1. Clone the repo in your $HOME directory
2. Run oh-my-zsh install script
3. `cd` into the repo
4. Run git submodule init && git submodule update
5. Run `stow -v .`
